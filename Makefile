export CGO_ENABLED=0

APP_VERSION = 0.0.1

.PHONY:
all: prepare install docker pkg test

.PHONY:
prepare:
	go get github.com/fullstorydev/grpcui/...

.PHONY:
install:
	go build -o assets/grpcui github.com/fullstorydev/grpcui/cmd/grpcui
	go build -o bin/goui cmd/main.go

.PHONY:
docker: 
	podman build -t bitbucket.org/s-gaur.io/grpc-goui:${APP_VERSION} -f Dockerfile .

.PHONY:
pkg:
	mkdir -p target/pkg
	cp -r bin assets templates target
	podman save bitbucket.org/s-gaur.io/grpc-goui:${APP_VERSION} -o target/pkg/goui-${APP_VERSION}.tar
	helm package helm/goui -d target/pkg
	mkdir -p target/tar
	tar -czvf goui-bundle.tar.gz target/assets target/bin target/pkg target/templates 

test:
	podman run -itd --rm -p 8080:8080 --name grpc-goui bitbucket.org/s-gaur.io/grpc-goui:${APP_VERSION}

.PHONY:
clean:
	rm -rf bin/goui assets/grpcui target
	rm -f goui-bundle.tar.gz 
	podman stop grpc-goui
	podman rmi -f bitbucket.org/s-gaur.io/grpc-goui:${APP_VERSION}

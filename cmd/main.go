package main

import (
	"bitbucket.org/s-gaur/grpc-goui/internal/app"
)

func main() {
	app.StartApplication()
}

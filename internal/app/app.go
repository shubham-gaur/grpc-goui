package app

import (
	"flag"

	"bitbucket.org/s-gaur/grpc-goui/internal/services/templates"
	"github.com/gin-gonic/gin"
)

var (
	router = gin.Default()
	server = flag.String("server", ":8080", "Accepted server listen IP :<port> or <ip>:<port>")
)

// StartApplication ...
func StartApplication() {
	flag.Parse()
	router.HTMLRender = templates.UserTemplate.CreateUserTemplate()
	mapURLs()
	router.Run(*server)
}

module bitbucket.org/s-gaur/grpc-goui

go 1.16

require (
	github.com/fullstorydev/grpcui v1.3.1 // indirect
	github.com/gin-contrib/multitemplate v0.0.0-20210428235909-8a2f6dd269a0
	github.com/gin-gonic/gin v1.7.1
	github.com/kr/pretty v0.1.0 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
)
